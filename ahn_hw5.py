import math
import numpy as np


def bisection (f, a, b, TOL=10.0*10**(-5), NMAX=20):
    i=0
    while i< NMAX:
        c = (a+b)/2.0
    
        if f(c)==0 or (b-a)/2.0 <= TOL:
            return c

        else:
            i = i+1
            if f(c)*f(a) > 0:
                a=c
                
            else:
                b=c 
    return False

def f(x):
    return np.sin(x)

need = bisection (f,2,4)
print need

def g(x): 
    return x**3-x-2

sleep = bisection (g,1,2)
print sleep

def y(x):
    return -6+x+(x**2)

now = bisection (y,0,5)
print now


        
