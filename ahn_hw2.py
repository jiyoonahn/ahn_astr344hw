#PRACTICE PROBLEM
import numpy as np
import math
x = np.float64 (1.e200)
y = np.float32 (10.)

print x
print y


import numpy as np
import math
x = np.float64 (1.e200)
y = np.float32 (1.e200)

print x
print y
#############################
#PROBLEM 1: Compute absolute and relative error with respect to what you obtain with floating point numbers.
import numpy as np
import math
for n in range (0,6):
	s = 1.0/3
	a = (s)**n
print a

import numpy as np
import math

x = np.float64 (a)
y = np.float32 (a)

print x
print y
##################################
#PROBLEM 1-A: How big are relative and absolute error at n = 5?
#ABSOLUTE ERROR:
#e(x) = x-xbar = np.float64 (a) - np.float32 (a)

e = x - y
print e

#RELATIVE ERROR:
#Er(X) = (x-xbar)/x = (np.float (a) - np.float32 (a))/np.float (a)

Er = (x - y) / x
print Er

###################################
#PROBLEM 2: What if you go to n = 20?
import numpy as np
import math
for n in range (0,21):
	s = 1.0/3
	a = (s)**n
print a

import numpy as np
import math

x = np.float64 (a)
y = np.float32 (a)

print x
print y

#ABSOLUTE ERROR
e = x - y
print e

#RELATIVE ERROR
Er = (x-y)/x
print Er
##########################################
#PROBLEM 3: Now do the same for x1 = 4, and compare it to xn = 4^n. Go to n = 20
import numpy as np
import math
for n in range (0,21):
	s = 4.0
	a = (s)**n
print a

import numpy as np
import math

x = np.float64 (a)
y = np.float32 (a)

print x
print y

#ABSOLUTE ERROR
e = x - y
print e

#RELATIVE ERROR
Er = (x - y)/x
print Er
#############################################
#PROBLEM 4: Why is this calculation stable?
Because 4 is an integer.
#############################################
#PROBLEM 5: Should absolute or relative error be used to measure accuracy and stability?
Yes, for non integers because it provides the absolute and relative errors. However for integers, finding the relative and absolute error value is a meaningless job.




